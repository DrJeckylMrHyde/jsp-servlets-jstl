package pl.sdacademy.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebFilter(filterName = "AuthorizationFilter", urlPatterns = {"/*"})
public class AuthorizationFilter implements Filter {

    private List<String> allowedURL = new ArrayList<>(Arrays.asList(
            "/index",
            "/index.jsp",
            "/home",
            "/login",
            "/login.jsp",
            "/register",
            "/register.jsp"
    ));

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

//        sprawdzamy czy istnieje sesja w ten sposob wiemy ze uzytkownik jest zalogowany
        HttpSession session = request.getSession(false);
        String uri = request.getRequestURI();
        System.out.println(allowedURL.contains(uri));

        if (session != null && session.getAttribute("userLogin") != null) {
            chain.doFilter(req, resp);
        } else {
            if (allowedURL.contains(uri)) {
                chain.doFilter(req, resp);
            } else {
                response.sendRedirect("index.jsp");
            }
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

    private void log(String info) {
        System.out.println("[AuthorizationFilter] " + info);
    }

}
