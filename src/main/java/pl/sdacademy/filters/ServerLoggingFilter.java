package pl.sdacademy.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

//definiujemy urlPatterns czyli adresy dla ktorych sie uruchamia w tym przypadku dla wszystkich
@WebFilter(filterName = "ServerLoggingFilter", urlPatterns = {"/*"})
public class ServerLoggingFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;

        System.out.println("*******************************");
        log(request.getRequestURI());
        log(request.getRequestURL().toString());
        log(request.getMethod());
        request.getParameterMap().entrySet().stream().forEach(parameter -> {
            System.out.println("Name:" + parameter.getKey() + ", Value: " + Arrays.toString(parameter.getValue()));
        });

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

    private void log(String info) {
        System.out.println("[ServerLoggingFilter] " + info);
    }

}
