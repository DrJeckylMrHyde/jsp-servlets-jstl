package pl.sdacademy.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Gallery {

    String name;
    List<Image> images;
    String date;

    public Gallery(String name) {
        this.name = name;
        this.images = new ArrayList<>();
        this.date = LocalDate.now().toString();
    }

    public String getName() {

        return this.name;
    }

    public List<Image> getImages() {

        return this.images;
    }

    public String getDate() {
        return this.date;
    }

    public void addToGalleryList(Image image){
        images.add(image);
    }
}
