package pl.sdacademy.model;

public class Image {

    private String URLadress;
    private String description;

    public Image(String URLadress, String description) {
        this.URLadress = URLadress;
        this.description = description;
    }

    public String getURLadress() {
        return URLadress;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Image{" +
                "description='" + description + '\'' +
                ", URLadress='" + URLadress + '\'' +
                '}';
    }
}
