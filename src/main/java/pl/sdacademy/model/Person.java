package pl.sdacademy.model;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private String name;
    private String lastName;
    private int bornYear;
    private int phoneNumber;
    private Sex sex;
    public static List<Person> personList = new ArrayList<>();

    public Person(String name, String lastName, int bornYear, int phoneNumber, Sex sex) {
        this.name = name;
        this.lastName = lastName;
        this.bornYear = bornYear;
        this.phoneNumber = phoneNumber;
        this.sex = sex;
    }

    public String getName() {

        return name;
    }

    public String getLastName() {

        return lastName;
    }

    public int getBornYear() {

        return bornYear;
    }

    public int getPhoneNumber() {

        return phoneNumber;
    }

    public Sex getSex() {

        return sex;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBornYear(int bornYear) {
        this.bornYear = bornYear;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public static void setPersonList(List<Person> personList) {
        Person.personList = personList;
    }
}