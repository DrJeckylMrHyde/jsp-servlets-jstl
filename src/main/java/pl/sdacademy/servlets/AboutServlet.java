package pl.sdacademy.servlets;

import pl.sdacademy.model.Person;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static pl.sdacademy.model.Person.personList;
import static pl.sdacademy.model.Sex.*;

//po przecinku przypisujemy adres
@WebServlet(name = "AboutServlet", value = "/about")
public class AboutServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Person Ania = new Person("Ania", "Bania", 1986, 723423234, FEMALE);
        Person Piotr = new Person("Piotr", "Zietek", 1992, 323423234, MALE);
        Person Kasia = new Person("Kasia", "Konczyk", 1988, 324345344, FEMALE);
        Person Przemo = new Person("Przemo", "Kariacik", 1987, 323425534, MALE);
        Person Lukasz = new Person("Lukasz", "Trojanowicz", 1992, 444423234, MALE);

        personList.add(Ania);
        personList.add(Piotr);
        personList.add(Kasia);
        personList.add(Przemo);
        personList.add(Lukasz);

//        "AboutPersonList" to key mapy metody setAttribute"
        request.setAttribute("AboutPersonList", personList);

        request.getRequestDispatcher("about.jsp").forward(request, response);
    }
}
