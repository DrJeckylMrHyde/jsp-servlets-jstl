package pl.sdacademy.servlets;

import pl.sdacademy.model.Person;
import pl.sdacademy.model.Sex;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "AddPersonServlet", value = "/addPersonServlet")
public class AddPersonServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/test.db");

            preparedStatement = connection.prepareStatement("INSERT INTO people VALUES(?,?,?,?,?);");

//                tu numerujemy od 1
                preparedStatement.setString(1, request.getParameter("name"));
                preparedStatement.setString(2, request.getParameter("surname"));
                preparedStatement.setInt(3, Integer.parseInt(request.getParameter("birthDate")));
                preparedStatement.setInt(4, Integer.parseInt(request.getParameter("phone")));
                preparedStatement.setString(5, request.getParameter("sex"));

                preparedStatement.executeUpdate();

        } catch (SQLException | ClassNotFoundException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
//        request.setAttribute("personList", personList);
        response.sendRedirect("showPerson");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("addPerson.jsp");
    }
}
