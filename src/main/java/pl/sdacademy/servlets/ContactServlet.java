package pl.sdacademy.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//po przecinku przypisujemy adres
@WebServlet(name = "ContactServlet", value = "/contact")

public class ContactServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        String userName = request.getParameter("user_name");
        String userSurname = request.getParameter("user_surname");
        String userEmail = request.getParameter("user_email");
        String userMessage = request.getParameter("message");

        userMessage = userMessage.replaceAll("\n", "<br>");

        request.setAttribute("formName", userName);
        request.setAttribute("formSurname", userSurname);
        request.setAttribute("formEmail", userEmail);
        request.setAttribute("formMessage", userMessage);

        request.getRequestDispatcher("sendMessage.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        System.out.println("Redirecting to index.jsp");
//        przekierowanie uzytkownika do konkretnej strony
//        w parametrze metody getParameter nadaje nazwę parametru przez który będę się odwoływał w adresie URL
//        String action = request.getParameter("action");
        response.sendRedirect("contact.jsp");
    }
}
