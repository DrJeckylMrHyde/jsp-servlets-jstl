package pl.sdacademy.servlets;

import pl.sdacademy.model.Gallery;
import pl.sdacademy.model.Image;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//po przecinku przypisujemy adres
@WebServlet(name = "GalleryServlet", value = "/gallery")
public class GalleryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        System.out.println("Redirecting to index.jsp");
//        przekierowanie uzytkownika do konkretnej strony
//        w parametrze metody getParameter nadaje nazwę parametru przez który będę się odwoływał w adresie URL
//        String action = request.getParameter("action");
//        response.sendRedirect("gallery.jsp");

//        pole w klasie inicjowane przez konsturktor klasy gallery
        Gallery gallery = new Gallery("theSimponsGallery");
        Image img1 = new Image("https://vignette.wikia.nocookie.net/simpsons/images/0/0c/Homer_simpson_and_donut-1090.png/revision/latest/scale-to-width-down/250?cb=20110911170400&path-prefix=pl", "Homer Simpson");
        Image img2 = new Image("https://vignette.wikia.nocookie.net/simpsons/images/f/fc/222px-Barney_Gumble.png/revision/latest?cb=20110913150341&path-prefix=pl", "Barney Gumble");
        Image img3 = new Image("https://vignette.wikia.nocookie.net/simpsons/images/b/b0/446px-Montgomery_Burns.png/revision/latest?cb=20110913130516&path-prefix=pl", "Mr Burns");

        gallery.addToGalleryList(img1);
        gallery.addToGalleryList(img2);
        gallery.addToGalleryList(img3);

//        (nazwa obiektu, obiekt)
//        (nazwa ktora przkeazuje do requesta)
        request.setAttribute("theSimponsGallery", gallery);

//        przejscie z obiektem do strony
        request.getRequestDispatcher("gallery.jsp").forward(request, response);
    }
}
