package pl.sdacademy.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//po przecinku przypisujemy adres
@WebServlet(name = "HomeServlet", value = "/home")
public class HomeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        System.out.println("Redirecting to index.jsp");
//        przekierowanie uzytkownika do konkretnej strony
//        w parametrze metody getParameter nadaje nazwę parametru przez który będę się odwoływał w adresie URL
        response.sendRedirect("index.jsp");

    }
}

