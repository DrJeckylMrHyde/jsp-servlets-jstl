package pl.sdacademy.servlets;

import pl.sdacademy.model.Gallery;
import pl.sdacademy.utils.InstagramDownloader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "InstagramServlet", value = "/instagram")
public class InstagramServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("instagram.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String instagramUserName = request.getParameter("instagram_user_name");
        String number = request.getParameter("number");
        int numberAsInt = Integer.valueOf(number);

        InstagramDownloader instagramDownloader = new InstagramDownloader(instagramUserName,numberAsInt);
        Gallery gallery = instagramDownloader.downloadPhotos();

        request.setAttribute("theSimponsGallery",gallery);
        request.getRequestDispatcher("gallery.jsp").forward(request, response);
    }
}
