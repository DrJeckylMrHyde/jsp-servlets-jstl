package pl.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        przekierowanie na strone logowania
        response.sendRedirect("login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String loginFromForm = request.getParameter("login");
        String passwordFromForm = request.getParameter("password");
        Connection connection = null;

        try {
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/museums.db");

            String sqlQuery
                    = "SELECT * FROM users WHERE login='" + loginFromForm
                    + "' AND password ='" + passwordFromForm + "';";

            PreparedStatement preparedStatement
                    = connection.prepareStatement(sqlQuery);

            ResultSet resultSet = preparedStatement.executeQuery();

//            przypadek gdy nie udalo sie znalezc uzytkownika w bazie
            if (!resultSet.next()) {
                String loginMessage
                        = "<p style=\"color:red\">Login or password error</p>";
//            aby tekst pojawil sie w przegladarce nalezy nadac atrybut ktory bedzie mozna pobrac
                request.setAttribute("loginMessage", loginMessage);
                request.getRequestDispatcher("login.jsp").forward(request, response);
//                znaleziono uzytkownika w bazie
            } else {
//                tworzymy nowa sesje i ustawiamy na user login
                HttpSession session = request.getSession();
//                ustawiamy na 20 sekund
                session.setMaxInactiveInterval(20);
//                ustawiamy atrybut na login z formularza o tej samej nazwie co w menu.jsp aby byl uniwersalny
                session.setAttribute("userLogin", loginFromForm);
//                przekierowuje do strony glownej
                response.sendRedirect("index.jsp");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
