package pl.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "LogoutServlet", value = "/logout")
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        jezeli sesja istnieje to zostanie zwrocona, jesli nie istniala to nowa nie zostanie stworzona
        HttpSession session = request.getSession(false);
//        jezeli powyzszy warunek zostal spelniony to sesja nie jest nullem, wtedy ubijamy sesje
        if(session != null){
            session.invalidate();
        }
        response.sendRedirect("index.jsp");
    }
}
