package pl.sdacademy.servlets;

import pl.sdacademy.model.Museum;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "MuseumServlet", value = "/museumServlet")
public class MuseumServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Museum> museumList = new ArrayList<>();

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/museums.db");
            String sqlQuery = "select * from museum where "+request.getParameter("searchBy")
                    +" like '%"+request.getParameter("input")+"%'";
            System.out.println(sqlQuery);
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer column1Value = resultSet.getInt(1);
                String column2Value = resultSet.getString(2);
                String column3Value = resultSet.getString(3);
                String column4Value = resultSet.getString(4);
                String column5Value = resultSet.getString(5);
                String column6Value = resultSet.getString(6);
                String column7Value = resultSet.getString(7);
                String column8Value = resultSet.getString(8);
                String column9Value = resultSet.getString(9);
                String column10Value = resultSet.getString(10);
                String column11Value = resultSet.getString(11);


                Museum museum = new Museum(column1Value, column2Value, column3Value, column4Value, column5Value,
                        column6Value, column7Value, column8Value, column9Value, column10Value, column11Value);

                museumList.add(museum);
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        request.setAttribute("museumList", museumList);
        request.getRequestDispatcher("museum.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("museum.jsp");
    }
}
