package pl.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "RegisterServlet", value = "/register")
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("register.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String loginFromRegisterForm = request.getParameter("login");
        String passwordFromRegisterForm = request.getParameter("password");
        String rePasswordFromRegisterForm = request.getParameter("rePassword");

        if (passwordFromRegisterForm.equals(rePasswordFromRegisterForm)) {

            if (register(loginFromRegisterForm, passwordFromRegisterForm) == 1) {
                String registerMessage
                        = "<p style=\"color:green\">Succesfully registered user "
                        + loginFromRegisterForm + "</p>";

//            aby tekst pojawil sie w przegladarce nalezy nadac atrybut ktory bedzie mozna pobrac
                request.setAttribute("loginMessage", registerMessage);
                request.getRequestDispatcher("login.jsp").forward(request, response);
            } else {
                String registerMessage
                        = "<p style=\"color:red\">User already exists "
                        + loginFromRegisterForm + "</p>";

//            aby tekst pojawil sie w przegladarce nalezy nadac atrybut ktory bedzie mozna pobrac
                request.setAttribute("registerMessage", registerMessage);
                request.getRequestDispatcher("register.jsp").forward(request, response);
            }

        } else {
            String registerMessage = "<p style=\"color:red\">Passwords do not match!</p>";
//            aby tekst pojawil sie w przegladarce nalezy nadac atrybut ktory bedzie mozna pobrac
            request.setAttribute("registerMessage", registerMessage);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }

    }

    private int register(String loginFromRegisterForm, String passwordFromRegisterForm) {

        int insertedRows = 0;

        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection
                    = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/museums.db");

            PreparedStatement preparedStatement
                    = connection.prepareStatement("INSERT INTO users VALUES(?,?);");

            preparedStatement.setString(1, loginFromRegisterForm);
            preparedStatement.setString(2, passwordFromRegisterForm);

            insertedRows = preparedStatement.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("User already exists: " + loginFromRegisterForm + ", " + e.getMessage());
        }
        return insertedRows;
    }
}

