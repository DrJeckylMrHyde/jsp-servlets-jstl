package pl.sdacademy.utils;

import org.json.JSONArray;
import org.json.JSONObject;
import pl.sdacademy.model.Gallery;
import pl.sdacademy.model.Image;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InstagramDownloader {

    private String instagramUserName;
    private int numberOfPhotos;
    private final String INSTAGRAM_URL = "https://instagram.com/";

    public InstagramDownloader(String instagramUserName, int numberOfPhotos) {
        this.instagramUserName = instagramUserName;
        this.numberOfPhotos = numberOfPhotos;
    }

    public Gallery downloadPhotos() {
        String pageSource = downloadPageSource();
        String jsonAsString = extractJsonAsString(pageSource);
        List<String> imagesList = extractPhotoListFromJson(jsonAsString);
        return createGallery(imagesList);
    }

    private String downloadPageSource() {
        String source = "";

        try {

            URL url = new URL(INSTAGRAM_URL + instagramUserName);
//            metoda służąca połączeniu się ze stroną internetową
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            return bufferedReader.lines().collect(Collectors.joining());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return source;
    }

    private String extractJsonAsString(String pageSource) {
        String[] splitPageSource = pageSource.split("window._sharedData = ");
        return splitPageSource[1].split(";</script>")[0];
//        return extractedJsonAsString;
    }

    private List<String> extractPhotoListFromJson(String jsonAsString) {
        List<String> imagesLinks = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(jsonAsString);
        JSONArray jsonArray = jsonObject.getJSONObject("entry_data").getJSONArray("ProfilePage").getJSONObject(0)
                .getJSONObject("graphql").getJSONObject("user").getJSONObject("edge_owner_to_timeline_media")
                .getJSONArray("edges");

        for (int i = 0; i < jsonArray.length(); i++){
            if(i < numberOfPhotos) {
                String imageLink = jsonArray.getJSONObject(i).getJSONObject("node").getString("display_url");
                imagesLinks.add(imageLink);
            }
        }

        return imagesLinks;
    }

    private Gallery createGallery(List<String> imagesLinks) {
        Gallery instagramGallery = new Gallery("instagram photos");
        for (String link : imagesLinks) {
            Image image = new Image(link, "Instagram photo");
            instagramGallery.addToGalleryList(image);
        }
        return instagramGallery;
    }
}
