<%@ page import="pl.sdacademy.model.Sex" %>
<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 24.02.2019
  Time: 11:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>About</title>
</head>
<body>
<h1>ABOUT</h1>
<jsp:include page="menu.jsp"/>
<p>Lista posiada: <c:out value="${fn:length(requestScope.AboutPersonList)}"/> osób</p>
<table border="1">
    <tr>
        <th width="100">Name:</th>
        <th width="100">LastName:</th>
        <th width="100">BornYear:</th>
        <th width="100">PhoneNumber:</th>
        <th width="100">Sex:</th>
    </tr>
    <%--  to jest zamiast <% Person person = (Person) request.getAttribute("AboutPersonList") --%>
    <%--  podaje tu atrybut na ktorym pracuje zdefiniowany w AboutServlet w tym przypadku jest to lista --%>
    <c:forEach items="${requestScope.AboutPersonList}" var="person">
        <tr>
            <td>
                <c:out value="${person.getName()}"/>
            </td>
            <td>
                <c:out value="${person.getLastName()}"/>
            </td>
            <td>
                <c:out value="${person.getBornYear()}"/>
            </td>
            <td>
                <c:out value="${person.getPhoneNumber()}"/>
            </td>
            <td>
                <c:out value="${person.getSex()}"/>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<br>
<table border="1">
    <c:forEach items="${requestScope.AboutPersonList}" var="person">
        <c:if test="${person.getSex() eq 'FEMALE'}">
            <tr>
                <td>
                    <c:out value="${person.getName()}"/>
                </td>
                <td>
                    <c:out value="${person.getLastName()}"/>
                </td>
                <td>
                    <c:out value="${person.getBornYear()}"/>
                </td>
                <td>
                    <c:out value="${person.getPhoneNumber()}"/>
                </td>
                <td>
                    <c:out value="${person.getSex()}"/>
                </td>
            </tr>
        </c:if>
    </c:forEach>

    <c:forEach items="${requestScope.AboutPersonList}" var="person">
        <%--        sposob przyrownania enuma --%>
        <c:if test="${person.getSex() eq 'MALE'}">
            <tr>
                <td>
                    <c:out value="${person.getName()}"/>
                </td>
                <td>
                    <c:out value="${person.getLastName()}"/>
                </td>
                <td>
                    <c:out value="${person.getBornYear()}"/>
                </td>
                <td>
                    <c:out value="${person.getPhoneNumber()}"/>
                </td>
                <td>
                    <c:out value="${person.getSex()}"/>
                </td>
            </tr>
        </c:if>
    </c:forEach>
</table>
<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate var="year" value="${now}" pattern="yyyy"/>
<br>
<br>
<table border="1">
    <c:forEach items="${requestScope.AboutPersonList}" var="person">
        <c:if test="${year-person.getBornYear() > 30}">
            <c:choose>
                <c:when test="${fn:startsWith(person.getLastName(),'K')}">
                    <tr style="color: green;">
                </c:when>
                <c:otherwise>
                    <tr>
                </c:otherwise>
            </c:choose>
            <td>
                <c:out value="${person.getName()}"/>
            </td>
            <td>
                <c:out value="${person.getLastName()}"/>
            </td>
            <td>
                <c:out value="${person.getBornYear()}"/>
            </td>
            <td>
                <c:out value="${person.getPhoneNumber()}"/>
            </td>
            <td>
                <c:out value="${person.getSex()}"/>
            </td>
            </tr>
        </c:if>
    </c:forEach>
</table>
</body>
</html>
