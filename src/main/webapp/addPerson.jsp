<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Person Form</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
<form action="addPersonServlet" method="post">
    <h3>Name:</h3>
    <input type="text" name="name"><br>
    <h3>Surname:</h3>
    <input type="text" name="surname"><br>
    <h3>Year of born:</h3>
    <input type="text" name="birthDate"><br>
    <h3>Phone:</h3>
    <input type="text" name="phone"><br>
    <h3>Sex:</h3>
    <input type="radio" name="sex" value="MALE" checked><br>
    <input type="radio" name="sex" value="FEMALE"><br>
    <input type="submit" value="Add person" name="submit">
</form>

</body>
</html>
