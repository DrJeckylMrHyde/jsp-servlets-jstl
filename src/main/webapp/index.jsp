<%--w pliku jsp można pisać zarówno w javie jak i html--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<%-- out w jsp to zmienna zdefiniowana czyli wbudowana na stale --%>
<h1>HOME</h1>
<jsp:include page="menu.jsp"/>
</body>
</html>
