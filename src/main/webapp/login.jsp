<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 17.03.2019
  Time: 09:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
<jsp:include page="menu.jsp"/>

<%--sprawdzamy czy atrybut wgl powstal jesli tak to wyswietlamy komunikat--%>
<c:if test="${requestScope.loginMessage != null}">
    ${requestScope.loginMessage}
</c:if>

    <form method="post" action="/login">
        <table>
            <tr>
                <td>Login:</td>
                <td><input type="text" name="login"></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Log in"></td>
            </tr>
        </table>
    </form>
</body>
</html>
