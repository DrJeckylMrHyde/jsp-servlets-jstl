<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Museum List</title>
</head>
<body>
<jsp:include page="menu.jsp"/>

<form action="museumServlet" method="post">
    <select name="searchBy">
        <option value="id">Lp</option>
        <option value="name">Nazwa</option>
        <option value="postalCode">Kod pocztowy</option>
        <option value="city">Miasto</option>
        <option value="streetPrefix">Pl./Al.</option>
        <option value="street">Ulica</option>
        <option value="houseNumber">Numer domu</option>
        <option value="flatNumber">Numer lokalu</option>
        <option value="entryDate">Data wpisu</option>
        <option value="status">Status</option>
    </select>
    <input type="text" name="input">
    <input type="submit" name="submitButton" value="search">
</form>

<h1>Musuems List:</h1>
<table border="1">
    <tr>
        <th>Lp</th>
        <th>Nazwa</th>
        <th>Kod pocztowy</th>
        <th>Miasto</th>
        <th>Pl./Al.</th>
        <th>Ulica</th>
        <th>Numer domu</th>
        <th>Numer lokalu</th>
        <th>Data wpisu</th>
        <th>Status</th>
    </tr>
    <c:forEach items="${requestScope.museumList}" var="museum">
        <tr>
            <td>
                <c:out value="${museum.getId()}"/>
            </td>
            <td>
                <c:out value="${museum.getName()}"/>
            </td>
            <td>
                <c:out value="${museum.getPostalCode()}"/>
            </td>
            <td>
                <c:out value="${museum.getCity()}"/>
            </td>
            <td>
                <c:out value="${museum.getStreetPrefix()}"/>
            </td>
            <td>
                <c:out value="${museum.getStreet()}"/>
            </td>
            <td>
                <c:out value="${museum.getHouseNumber()}"/>
            </td>
            <td>
                <c:out value="${museum.getFlatNumber()}"/>
            </td>
            <td>
                <c:out value="${museum.getOrganizer()}"/>
            </td>
            <td>
                <c:out value="${museum.getEntryDate()}"/>
            </td>
            <td>
                <c:out value="${museum.getStatus()}"/>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
