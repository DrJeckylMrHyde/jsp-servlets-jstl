<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 02.03.2019
  Time: 15:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <title>sendMessage</title>
</head>
<body>
<h4><c:out value="Hello ${requestScope.formName} ${requestScope.formSurname}"/></h4>
<h4><c:out value="Your email adress: ${requestScope.formEmail}"/></h4>
<p> You send message: <br> ${requestScope.formMessage}</p>
<a href="index.jsp">Back</a>
</body>
</html>
