<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 16.03.2019
  Time: 10:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Person List</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
<h1>Person List:</h1>
    <table border="1">
        <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Year of Born</th>
        <th>Phone</th>
        <th>Sex:</th>
        </tr>
        <c:forEach items="${requestScope.personList}" var="person">
            <tr>
                <td>
                    <c:out value="${person.getName()}"/>
                </td>
                <td>
                    <c:out value="${person.getLastName()}"/>
                </td>
                <td>
                    <c:out value="${person.getBornYear()}"/>
                </td>
                <td>
                    <c:out value="${person.getPhoneNumber()}"/>
                </td>
                <td>
                    <c:out value="${person.getSex()}"/>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
