<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 24.02.2019
  Time: 11:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html lang="pl">
<head>
<meta charset="utf-8"/>
    <title>Contact</title>
</head>
<body>
<h1>CONTACT</h1>
<jsp:include page="menu.jsp"/>

<%-- action = "adres serwletu" --%>
<form method="post" action="/contact">
    <table>
        <tr>
            <td><h4>Name</h4></td>
            <td><input type="text" name="user_name"></td>
        </tr>
        <tr>
            <td><h4>Surname</h4></td>
            <td><input type="text" name="user_surname"></td>
        </tr>
        <tr>
            <td><h4>Email</h4></td>
            <td><input type="email" name="user_email"></td>
        </tr>
        <tr>
            <td><h4>Message</h4></td>
            <td><textarea rows = "20" cols = "50" type="text" name="message"></textarea></td>
        </tr>
        <tr>
            <td><h4>Send</h4></td>
            <td><input type="submit" value="Send message!"></td>
        </tr>
    </table>
</form>
</body>
</html>
