<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 24.02.2019
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Gallery</title>
</head>
<body>
<h1>GALLERY</h1>
<jsp:include page="menu.jsp"/>
<%--<% Gallery gallery = (Gallery) request.getAttribute("theSimponsGallery"); %>--%>
<h4>Name: <c:out value="${requestScope.theSimponsGallery.getName()}"/>
</h4>
<h4>Created: <c:out value="${requestScope.theSimponsGallery.getDate()}"/>
</h4>
<table border="1">
    <tr>
<%--    <%--%>
<%--        for (Image img : gallery.getImages()) {--%>
<%--    %>--%>
    <c:forEach items="${requestScope.theSimponsGallery.getImages()}" var="photo">
        <td><img src="${photo.getURLadress()}" title="${photo.getDescription()}" width="250px" height="330"></td>
    </c:forEach>
<%--    <%}%>--%>
    </tr>
</table>
</body>
</html>
