<%--tutaj bez body--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--sprawdzam czy istnieje ciasteczko wtedy wiem ze uzytkownik poprawnie sie zalogowal
wtedy wyswietlamy komunikat Hello [login]--%>
<c:if test="${sessionScope.userLogin != null}">
    <p style="color: green">Hello <c:out value="${sessionScope.userLogin}"/></p>
</c:if>

<h4>menu</h4>
<ul>
    <%--    tu przekazuje value z Servletow --%>
    <li><a href="home" target="_blank">home</a></li>
    <li><a href="gallery" target="_blank">gallery</a></li>
    <li><a href="about" target="_blank">about</a></li>
    <li><a href="contact" target="_blank">contact</a></li>
    <li><a href="instagram" target="_blank">instagram</a></li>
    <li><a href="addPersonServlet" target="_blank">addPerson</a></li>
    <li><a href="showPerson" target="_blank">showPersons</a></li>
    <li><a href="museumServlet" target="_blank">museums</a></li>
</ul>

<h4>Login menu</h4>
<ul>
    <%--te warunki służą ustawienia aby użytkownik nie widział login gdy jest już zalogowany
    oraz logout gdy jest wylogowany--%>
    <c:if test="${sessionScope.userLogin == null}">
        <li><a href="login">Login</a></li>
    </c:if>
    <c:if test="${sessionScope.userLogin != null}">
        <li><a href="logout">Logout</a></li>
    </c:if>
        <li><a href="register">Rejestracja</a></li>
</ul>